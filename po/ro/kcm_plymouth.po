# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plymouth-kcm package.
# Sergiu Bivol <sergiu@cip.md>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: plymouth-kcm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-07 01:01+0000\n"
"PO-Revision-Date: 2020-09-19 09:21+0100\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 19.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Sergiu Bivol"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sergiu@cip.md"

#: helper.cpp:57 helper.cpp:110 helper.cpp:138 helper.cpp:315
#, kde-format
msgid "Cannot start update-alternatives."
msgstr "Nu se poate porni „update-alternatives”."

#: helper.cpp:62 helper.cpp:115 helper.cpp:143 helper.cpp:320
#, kde-format
msgid "update-alternatives failed to run."
msgstr "Rularea update-alternatives a eșuat."

#: helper.cpp:69 helper.cpp:123 helper.cpp:150 helper.cpp:328
#, kde-format
msgid "update-alternatives returned with error condition %1."
msgstr "„update-alternatives” a întors condiția de eroare %1."

#: helper.cpp:82
#, kde-format
msgid "No theme specified in helper parameters."
msgstr "Nicio tematică specificată în parametrii ajutători."

#: helper.cpp:100 helper.cpp:259 helper.cpp:305
#, kde-format
msgid "Theme corrupted: .plymouth file not found inside theme."
msgstr ""
"Tematică coruptă: fișierul „plymouth” nu a fost găsit in interiorul "
"tematicii."

#: helper.cpp:161
#, kde-format
msgid "Cannot start initramfs."
msgstr "Nu se poate porni „initramfs”."

#: helper.cpp:169
#, kde-format
msgid "Initramfs failed to run."
msgstr "Rularea „initramfs” a eșuat."

#: helper.cpp:179
#, kde-format
msgid "Initramfs returned with error condition %1."
msgstr "„initramfs” a întors condiția de eroare %1."

#: helper.cpp:289
#, kde-format
msgid "Theme folder %1 does not exist."
msgstr "Dosarul cu tematici %1 nu există."

#: helper.cpp:295
#, kde-format
msgid "Theme %1 does not exist."
msgstr "Tematica %1 nu există."

#: kcm.cpp:41
#, kde-format
msgid "Boot Splash Screen"
msgstr "Ecran de demarare"

#: kcm.cpp:42
#, kde-format
msgid "Marco Martin"
msgstr "Marco Martin"

#: kcm.cpp:202 kcm.cpp:222
#, kde-format
msgid "Unable to authenticate/execute the action: %1 (%2)"
msgstr "Imposibil de autentificat/executat acțiunea: %1 (%2)"

#: kcm.cpp:226
#, kde-format
msgid "Theme uninstalled successfully."
msgstr "Tematică dezinstalată cu succes."

#: kplymouththemeinstaller.cpp:32
#, kde-format
msgid "Plymouth theme installer"
msgstr "Instalator de tematici Plymouth"

#: kplymouththemeinstaller.cpp:38
#, kde-format
msgid "Install a theme."
msgstr "Instalează o tematică."

#: kplymouththemeinstaller.cpp:39
#, kde-format
msgid "Uninstall a theme."
msgstr "Dezinstalează o tematică."

#: kplymouththemeinstaller.cpp:41
#, kde-format
msgid "The theme to install, must be an existing archive file."
msgstr "Tematica de instalat, trebuie să fie un fișier de arhivă existent."

#: kplymouththemeinstaller.cpp:113
#, kde-format
msgid "Unable to authenticate/execute the action: %1, %2"
msgstr "Imposibil de autentificat/executat acțiunea: %1, %2"

#: package/contents/ui/main.qml:17
#, kde-format
msgid "This module lets you choose the Plymouth boot splash screen."
msgstr "Acest modul vă permite să alegeți ecranul de demarare Plymouth."

#: package/contents/ui/main.qml:21
#, kde-format
msgctxt "@action:button as in, get new Plymouth boot splash screens"
msgid "Get New…"
msgstr ""

#: package/contents/ui/main.qml:49
#, kde-format
msgid "Uninstall"
msgstr "Dezinstalează"

#~ msgid "Get New Boot Splash Screens..."
#~ msgstr "Obține ecrane de demarare noi..."

#~ msgid "Download New Boot Splash Screens"
#~ msgstr "Descarcă ecrane de demarare noi"
